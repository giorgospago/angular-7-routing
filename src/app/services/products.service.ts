import { Injectable } from '@angular/core';
import { IProduct } from '../interfaces/IProduct';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public products: IProduct[] = [
    {id: 1, name: 'Core i3'},
    {id: 2, name: 'Core i5'},
    {id: 3, name: 'Core i7'},
    {id: 4, name: 'Core i9'}
  ];

  constructor() { }
}
