import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import {IProduct} from '../../interfaces/IProduct';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  title: string = '';

  constructor(
    private route: ActivatedRoute,
    private ps: ProductsService
  ) { }

  ngOnInit() {
    const productId = this.route.snapshot.params.id;

    const a: IProduct = this.ps.products.find(x => x.id == productId);

    if (a) {
      this.title = a.name;
    } else {
      this.title = 'Product not found';
    }

  }

}
